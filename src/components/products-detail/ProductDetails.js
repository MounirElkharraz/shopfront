import React from 'react';
import { useParams } from 'react-router-dom';
import axios from 'axios';
import { useQuery } from 'react-query';
import './ProductDetails.css';
import 'bootstrap/dist/css/bootstrap.min.css';

const fetchProduct = async (id) => {
  const response = await axios.get(`https://fakestoreapi.com/products/${id}`);
  return response.data;
};

const ProductDetail = () => {
  const { id } = useParams();

  const { data: product, isLoading, error } = useQuery(['product', id], () =>
    fetchProduct(id)
  );

  if (isLoading) {
    return <div>Loading...</div>;
  }

  if (error) {
    return <div>Error fetching product</div>;
  }

  return (
    <div className="container bootdey">
      <div className="col-md-12">
        <section className="panel">
          <div className="panel-body">
            <div className="col-md-6">
              <div className="pro-img-details">
                <img src={product.image} alt={product.title} />
              </div>
            </div>
            <div className="col-md-6">
              <h4 className="pro-d-title">
                <a href="#/" className="">
                  {product.title}
                </a>
              </h4>
              <p>{product.description}</p>
              <div className="product_meta">
                <span className="posted_in">
                  <strong>Category:</strong> {product.category}
                </span>
                <span className="tagged_as">
                  <strong>Tags:</strong>{' '}
                  {product.tags ? (
                    product.tags.map((tag, index) => (
                      <a key={index} rel="tag" href="#/">
                        {tag}
                      </a>
                    ))
                  ) : (
                    <span>No tags available</span>
                  )}
                </span>
              </div>
              <div className="m-bot15">
                <strong>Price : </strong>{' '}
                <span className="amount-old">${product.price}</span>{' '}
                <span className="pro-price"> ${product.price}</span>
              </div>
              <div className="form-group">
                <label>Quantity</label>
                <input
                  type="quantiy"
                  placeholder="1"
                  className="form-control quantity"
                />
              </div>
              <p>
                <button className="btn btn-round btn-danger" type="button">
                  <i className="fa fa-shopping-cart"></i> Add to Cart
                </button>
              </p>
            </div>
          </div>
        </section>
      </div>
    </div>
  );
};

export default ProductDetail;
