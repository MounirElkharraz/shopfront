import React ,{ useContext }from 'react';
import { useQuery } from 'react-query';
import axios from 'axios';
import { Link } from 'react-router-dom';
import './ProductList.css';
import 'bootstrap/dist/css/bootstrap.min.css';
import { ShopContext } from '../context/ShopContext';

const fetchProducts = async () => {
  try {
    const response = await axios.get('https://fakestoreapi.com/products');
    return response.data;
  } catch (error) {
    throw new Error('Error fetching products');
  }
};

function ProductList() {
  const { data: products, isLoading, isError } = useQuery('products', fetchProducts);
  const { addToCart, cartItems } = useContext(ShopContext);
  let cartItemCount = 0; 

  if (products) {
    cartItemCount = cartItems[products.id];
  }
  if (isLoading) {
    return <div>Loading...</div>;
  }

  if (isError) {
    return <div>Error fetching data</div>;
  }

  const productRows = [];
  for (let i = 0; i < products.length; i += 3) {
    const rowProducts = products.slice(i, i + 3);
    const productRow = (
      <div className="row" key={i}>
        {rowProducts.map((product) => (
          <div className="col-md-4" key={product.id}>
            <div className="ibox">
              <div className="ibox-content product-box">
                <div className="product-imitation">
                  <img src={product.image} alt={product.title} style={{ width: '100%', height: '150px', objectFit: 'contain' }} />
                </div>
                <div className="product-desc">
                  <span className="product-price">
                    ${product.price}
                  </span>
                  <small className="text-muted">{product.category}</small>
                  <Link to={`/products/${product.id}`} className="product-name">{product.title}</Link>
                  <div className="small m-t-xs">
                    {product.description}
                  </div>
                  <div className="m-t text-right">
                    <Link to={`/products/${product.id}`} className="btn btn-xs btn-outline btn-primary">Info <i className="fa fa-long-arrow-right"></i> </Link>
                    <button  className="btn btn-xs btn-outline btn-primary" onClick={()=>addToCart(product.id)}>
                    Add To Cart {cartItemCount > 0 && <> ({cartItemCount})</>}</button>
                  </div>
                </div>
              </div>
            </div>
          </div>
        ))}
      </div>
    );
    productRows.push(productRow);
  }

  return <div className="container">{productRows}</div>;
}

export default ProductList;
