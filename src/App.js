import React from 'react';
import { QueryClient, QueryClientProvider } from 'react-query';
import ProductList from './components/ProductList';
import ProductDetail from './components/products-detail/ProductDetails';
import ShoppingCart from './components/shopping-cart/ShoppingCart';
import { BrowserRouter as Router, Route, Routes, Link } from 'react-router-dom';
import { Navbar } from './components/navbar/Navbar';
import { ShopContextProvider } from './context/ShopContext';
const queryClient = new QueryClient();

function App() {
  return (
    <QueryClientProvider client={queryClient}>
      <div className="App">
        <ShopContextProvider>
        <Router>
          <Navbar/>
          <Routes>
            <Route path="/" element={<ProductList/>} />
            <Route path="/products/:id" element={<ProductDetail/>} />
            <Route path="/shopping" element={<ShoppingCart/>} />
          </Routes>
        </Router>

        </ShopContextProvider>
      
      </div>
    </QueryClientProvider>
  );
}

export default App;
